﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollItemBase : MonoBehaviour {

    public Image icon;

    string _data;

    public virtual void UpdateData(string data, int i)
    {
        Color c ;
        switch(i%3)
        {
            case 1:
                c = Color.red;
                break;
            case 2:
                c = Color.cyan;
                break;
            default:
                c = Color.yellow;
                break;
        }

        _data = data;
        GetComponentInChildren<Text>().text = _data;
        icon.color = c;
    }
	
}
