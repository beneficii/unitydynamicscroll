﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollRectOptimised : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {

    public float MomentumPreservanceRate = 0.85f;
    public GameObject ListItemPrefab;
    public RectTransform Content;

    List<string> _datas;
    List<ScrollItemBase> _items;

    float _uiItemSize;
    int _uiItemCount;

    int _prevFirstItemOffset = 0;
    float _potentialInertia = 0f;
    float _currentInertia = 0f;

    float _currentScroll;
    public float CurrentScroll
    {
        get { return _currentScroll; }
        set
        {
            _currentScroll = Mathf.Clamp(value, 0f, (_datas.Count - _uiItemCount + 2) * _uiItemSize);

            Content.anchoredPosition = new Vector2(0, CurrentScroll % _uiItemSize);

            int firstValue = Mathf.Clamp(Mathf.FloorToInt(CurrentScroll / _uiItemSize), 0, 100);
            if (_prevFirstItemOffset != firstValue)
            {
                RenderItems(firstValue);
            }
            _prevFirstItemOffset = firstValue;
        }
    }

    void Start()
    {
        if (_datas == null)
        {
            _datas = new List<string>()
            {
                "SomeItem",
                "Other Item",
                "Nice tm",
                "Cool thing",
                "Another one of these",
                "Take it pls",
                "Itemtemtem",
                "lul"
            };
        }
        /////////////// delet b4 dis

        Init();
        RenderItems(0);
    }

    public void Init()
    {
        _items = new List<ScrollItemBase>();

        _uiItemCount = 10; //just some temp value to start loop
        for (int i = 0; i < _uiItemCount; i++)
        {
            var item = Instantiate(ListItemPrefab, Content).GetComponent<ScrollItemBase>();
            _items.Add(item);

            if (i == 0)
            {
                _uiItemSize = item.GetComponent<RectTransform>().sizeDelta.y + Content.GetComponent<VerticalLayoutGroup>().spacing;
                _uiItemCount = Mathf.CeilToInt(Content.sizeDelta.y / _uiItemSize);
            }
        }
    }

    void RenderItems(int startIndex)
    {
        for (int uiIdx = 0; uiIdx < _uiItemCount; uiIdx++)
        {
            var dataIdx = startIndex + uiIdx;

            var item = _items[uiIdx];
            if (dataIdx >= _datas.Count)
            {
                item.gameObject.SetActive(false);
            }
            else
            {
                item.gameObject.SetActive(true);
                item.UpdateData(_datas[dataIdx], dataIdx);
            }

        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _currentInertia = _potentialInertia = 0f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        CurrentScroll += eventData.delta.y;
        _potentialInertia = eventData.delta.y;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _currentInertia = _potentialInertia;
    }

    void Update()
    {
        if (Mathf.Abs(_currentInertia) > 0.01f)
        {
            _currentInertia *= MomentumPreservanceRate;
            CurrentScroll += _currentInertia;
        }
    }

}
